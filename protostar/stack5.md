# Stack 5

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  char buffer[64];

  gets(buffer);
}
```

## Challenge

This is a classic buffer overflow challenge in which we need to inject our own shellcode and then modify the saved eip to point to that payload

we can find the offset using the same technique as in stack4 challenge and hence the offset is `72` to modify ebp and 76 to modify eip!
Now let us create our payload

before that we need to find the address where we will point our eip to for that let's fire up gdb and get the address of buffer

![](images/stack5_0.png)

clearly the buffer starts at `0xbffff700` as we will fill our buffer with nops and hence we should take the return address to the middle of nop sled to be on safe side so i will choose my return address to be `0xbffff710` and hence our payload will become as

![](images/stack5_1.png)

on directing the output of this script to payload and running the payload

![](images/stack5_2.png)

voila i am root!