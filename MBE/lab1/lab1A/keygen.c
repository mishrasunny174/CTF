#include <stdio.h>
#include <string.h>

int main(int argc, char** argv) {
	unsigned int serial;
	int counter=0;
	char username[100];
	unsigned int userNameLength;
	if (argc!=2) {
		printf("Usage: %s <username>\n",argv[0]);
		return 1;
	}
	if(strlen(argv[1])<6) {
		printf("ERROR: username should be of length more than 5");
		return 2;
	}
	strncpy(username,argv[1],99);
	userNameLength = strlen(username);
	serial = ((int)username[3] ^ 0x1337U) + 0x5eeded;
	while( counter<userNameLength ) {
		serial = serial + ((int)username[counter] ^ serial) % 0x539;
		counter++;
	}
	printf("serial: %u",serial);
	return 0;
}	
