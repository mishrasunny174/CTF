# Lab1C

After logging in to the machine we can go to /levels/level1 directory to see the file we need to exploit.

![00.png](images/00.png)

![01.png](images/01.png)

![02.png](images/02.png)

by preliminary analysis we can say that the the binary is 32bit binary non-stripped (i.e, in layman terms binary contains names of all the defined variables and functions) executeable and it asks for a password when we run it and if provide a wrong password it fails,
so now we can load the binary in gdb for more detailed analysis.

> ```gdb ./lab1C```

and then typing

> ```info functions```

to list all the functions in the binary

![03.png](images/03.png)

then using

> ```disas main```

to see the disassemble main function

![04.png](images/04.png)

clearly the input we are giving is being compared with 0x149a
so the correct password for this file is **0x149a** or **5274**

now running the binary and providing right password we can get the password for next level (lab1B) of lab1

![05.png](images/05.png)